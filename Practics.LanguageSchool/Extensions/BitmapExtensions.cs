﻿using System.Drawing;

namespace Practics.LanguageSchool.Extensions
{
    public static class BitmapExtensions
    {
        public static byte[] ImageToByte(this Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }
}