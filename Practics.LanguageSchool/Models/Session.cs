﻿namespace Practics.LanguageSchool.Models
{
    public static class Session
    {
        public static bool IsAdmin { get; set; }
        public static bool IsGuest { get; set; }
    }
}