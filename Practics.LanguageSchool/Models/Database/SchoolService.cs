﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Practics.LanguageSchool.Models.Database
{
    public class SchoolService
    {
        public SchoolService() { }
        
        public SchoolService(string serviceName, string mainImagePath, int durationInMinutes, decimal price, int? discountPercent)
        {
            ServiceName = serviceName;
            MainImagePath = mainImagePath;
            DurationInMinutes = durationInMinutes;
            Price = price;
            DiscountPercent = discountPercent;
        }

        public int Id { get; set; }
        public string ServiceName { get; set; }
        public string MainImagePath { get; set; }
        public int DurationInMinutes { get; set; }
        public decimal Price { get; set; }
        public int? DiscountPercent { get; set; }

        [NotMapped] public string DurationForView => $"{Price} рублей за {DurationInMinutes} минут";
        public string Discount => $"{(DiscountPercent.HasValue ? $"* скидка {DiscountPercent}%" : string.Empty)}";
    }
}