﻿using System;

namespace Practics.LanguageSchool.Models.Database
{
    public class SchoolClientService
    {
        public SchoolClientService() { }
        
        public SchoolClientService(int schoolClientId, int schoolServiceId, DateTime serviceStartedAt)
        {
            SchoolClientId = schoolClientId;
            SchoolServiceId = schoolServiceId;
            ServiceStartedAt = serviceStartedAt;
        }

        public int Id { get; set; }
        
        public int SchoolClientId { get; set; }
        public SchoolClient SchoolClient { get; set; }
        
        public int SchoolServiceId { get; set; }
        public SchoolService SchoolService { get; set; }
        
        public DateTime ServiceStartedAt { get; set; }
    }
}