﻿using System;
using Practics.LanguageSchool.Enums;

namespace Practics.LanguageSchool.Models.Database
{
    public class SchoolClient
    {
        public SchoolClient() { }
        
        public SchoolClient(string surname, string firstName, string secondName, Gender gender, string phoneNumber, string email, DateTime birthday, DateTime registrationDate)
        {
            Surname = surname;
            FirstName = firstName;
            SecondName = secondName;
            Gender = gender;
            PhoneNumber = phoneNumber;
            Email = email;
            Birthday = birthday;
            RegistrationDate = registrationDate;
        }

        public int Id { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public Gender Gender { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegistrationDate { get; set; }
    }
}