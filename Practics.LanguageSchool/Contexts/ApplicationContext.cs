﻿using System.Data.Entity;
using Practics.LanguageSchool.Models.Database;

namespace Practics.LanguageSchool.Contexts
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("School.Practics")
        {
            Database.CreateIfNotExists();
        }

        public ApplicationContext(string connection) : base(connection)
        {
            Database.CreateIfNotExists();
        }

        public DbSet<SchoolClient> SchoolClients { get; set; }
        public DbSet<SchoolClientService> SchoolClientServices { get; set; }
        public DbSet<SchoolService> SchoolServices { get; set; }
    }
}