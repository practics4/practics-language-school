﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using Practics.LanguageSchool.Contexts;
using Practics.LanguageSchool.Models.Database;

namespace Practics.LanguageSchool
{
    public partial class ServiceCatalogWindow : Window
    {
        private List<SchoolService> _services;
        
        public ServiceCatalogWindow()
        {
            InitializeComponent();
            
            InititalizeServices();
        }

        public void InititalizeServices()
        {
            using (var context = new ApplicationContext())
            {
                _services = context.SchoolServices.ToList();

                servicesListView.ItemsSource = _services;
            }
        }
    }
}